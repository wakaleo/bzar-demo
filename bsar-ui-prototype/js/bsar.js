function selectActive(inboxElement) {
    var selectedValue = $(inboxElement).attr("value")
    if (selectedValue == "pending" || selectedValue == "conditional") {
        $("input[value='draft']").prop('checked', false);
        $("input[value='approved']").prop('checked', false);
        $("input[value='rejected']").prop('checked', false);
    } else {
        $("input[value='pending']").prop('checked', false);
        $("input[value='conditional']").prop('checked', false);
    }
}

function applications() {
    return []  // TODO
}

function getAlterationsByType(types) {
    return []  // TODO
}

function getAlterationDetails(id) {
    return null // TODO
}

function getAlterationChanges(id) {
    return null // TODO
}

var changesDataSource = {
    data: [],
    pageSize: 8
};

function updateAlterationsList(alterationTypes) {
    var newAlterationList = getAlterationsByType(alterationTypes)
    var grid = $("#grid").data("kendoGrid");
    grid.dataSource.data(newAlterationList);
    grid.refresh();
    $("#alteration-details").hide()
}

function updateDetailsForAlteration(id, subject, requestDate) {
    $("#alteration-details").show()
    var details = getAlterationDetails(id)
    $("#details-request-id").text(id)
    $("#details-subject").text(subject)
    $("#details-description").text(details.description)
    $("#details-request-date").text(kendo.toString(requestDate, "dd-MMM-yyyy"))

    var newAlterationChanges = getAlterationChanges(id)
    var grid = $("#changes-grid").data("kendoGrid");
    grid.dataSource.data(newAlterationChanges);
    grid.refresh();
}


function selectedAlterationTypes() {
    var selectedAlternationTypes = [];
    $(".nav-list input:checked").each(function (index) {
        selectedAlternationTypes[index] = $(this).attr("value");
    });
    return selectedAlternationTypes
}
$(document).ready(function () {

    kendo.culture("en-AU");

    //----------------------------
    // Alteration selection list
    //----------------------------
    $(".nav-list input").click(function (event) {
        selectActive(this)
        updateAlterationsList(selectedAlterationTypes())
    });

    //----------------------------
    // Alterations grid
    //----------------------------
    $("#grid").kendoGrid({
        dataSource: {
            data: applications(),
            pageSize: 5

        },
        groupable: true,
        sortable: true,
        navigatable: true,
        selectable: "row",
        pageable: {
            refresh: true,
            pageSizes: true
        },
        filterable: {
            extra: false,
            operators: {
                string: {
                    startswith: "Starts with",
                    eq: "Is equal to",
                    neq: "Is not equal to",
                    contains: "Contains"
                }
            }
        },
        schema: {
            model: {
                fields: {
                    contract: { type: "string" },
                    subject: { type: "string" },
                    type: { type: "string" },
                    created: { type: "date" }
                }
            }
        },
        columns: [
            { field: "requestId", width: 80, title: "ID" } ,
            { field: "contract", width: 100, title: "Contract" } ,
            { field: "subject", title: "Subject" } ,
            { field: "type", width: 80, title: "Type",
                filterable: {
                    ui: typeFilter
                }
            },
            { field: "status", width: 100, title: "Status" ,
                filterable: {
                    ui: statusFilter
                }
            },
            { field: "created", width: 100, title: "Created",
                filterable: {
                    ui: "datetimepicker"
                },
                template: '#= kendo.toString(created,"dd-MMM-yy") #'}
        ]
    });
    var grid = $("#grid").data("kendoGrid");
    grid.bind("change", grid_change);

    //----------------------------
    // Changes grid
    //----------------------------
    $("#changes-grid").kendoGrid({
        dataSource: changesDataSource,
        sortable: true,
        pageable: {
            refresh: true,
            pageSizes: true
        },
        columns: [
            { field: "change", width: 120, title: "Change" } ,
            { field: "route", width: 60, title: "Route" } ,
            { field: "day", width: 80, title: "Day" } ,
            { field: "term", title: "Subject" } ,
            { field: "timeOfDay", width: 100, title: "Time of Day"},
            { field: "busType", width: 80, title: "Bus Type" },
            { field: "serviceTrips", width: 50, title: "Trips" },
            { field: "serviceKms", width: 50, title: "Kms" },
            { field: "serviceDuration", width: 80, title: "Time (min)" }
        ]
    });

    //----------------------------
    // Open the Alterations dialog
    //----------------------------
    $("#alterations-list").dblclick(function () {

        var alterations = $("#grid").data("kendoGrid");
        var selectedAlteration = grid.dataItem(grid.select()[0]);

        var win = $("#alteration-dialog").data("kendoWindow");
        win.center();
        win.open();
        win.title('Alteration ' + selectedAlteration.requestId + ' - ' + selectedAlteration.subject)
    });

    //----------------------------
    // Alterations dialog
    //----------------------------
    $("#alteration-dialog").kendoWindow({
        actions: ["Custom", "Refresh", "Maximize", "Minimize", "Close"],
                draggable: false,
                height: "500px",
                modal: true,
                resizable: false,
                title: "Modal Window",
                width: "800px"
    });

    $("#alterationTabs").kendoTabStrip();
});

statusValues = ["Pending","Conditionally Approved", "Approved","Rejected","Draft"]

types = ["Service"]

function typeFilter(element) {
    element.kendoDropDownList({
        dataSource: types
    });
}

function statusFilter(element) {
    element.kendoDropDownList({
        dataSource: statusValues
    });
}

function grid_change(e) {
    var selectedRows = this.select();
    var selectedDataItems = [];
    for (var i = 0; i < selectedRows.length; i++) {
        var dataItem = this.dataItem(selectedRows[i]);
        selectedDataItems.push(dataItem);
    }
    updateDetailsForAlteration(selectedDataItems[0].requestId, selectedDataItems[0].subject, selectedDataItems[0].created)
}
