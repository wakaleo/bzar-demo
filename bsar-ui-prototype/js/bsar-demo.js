// DEMO FUNCTIONS
var countries = [ "Australia", "Canada", "United States of America" ];

var countriesDataSource = new kendo.data.DataSource({
    data: countries
});

$(function() {
    $("#contractSearch").kendoAutoComplete({
         dataSource: countriesDataSource
    });
});


// RETURN DEMO APPLICATION DATA
function applications() {
    return getAlterationsByType(["pending","conditional"])
}

function getAlterationsByType(types) {
    var results = []
    if ($.inArray("pending", types) >= 0)  {
        results = results.concat(pending())
    }
    if ($.inArray("conditional", types) >= 0)  {
        results = results.concat(conditional())
    }
    if ($.inArray("approved", types) >= 0)  {
        results = results.concat(approved())
    }
    if ($.inArray("rejected", types) >= 0)  {
        results = results.concat(rejected())
    }
    if ($.inArray("draft", types) >= 0)  {
        results = results.concat(draft())
    }
    return results
}

function pending() {
    var rows = []
    rows.push( application(1008,"SMBSC014","Increase passenger loading on morning peak","Service", "Pending") );
    rows.push( application(1011,"SMBSC016","Increasing frequency for Hornsby","Service", "Pending") );
    rows.push( application(1013,"SMBSC017","Increasing frequency for Gosford","Service", "Pending") );
    rows.push( application(1015,"SMBSC018","Increasing frequency for Blacktown","Service", "Pending") );
    return rows
}

function conditional() {
    var rows = []
    rows.push( application(1016,"SMBSC014","Time table adjustments for Blacktown","Service", "Contitionally Approved") );
    rows.push( application(1001,"SMBSC014","Increase passenger loading on morning peak","Service", "Contitionally Approved") );
    rows.push( application(1051,"SMBSC014","Increase passenger loading on morning peak","Service", "Contitionally Approved") );
    rows.push( application(1053,"SMBSC014","Increasing frequency for Gosford","Service", "Contitionally Approved") );
    rows.push( application(1055,"SMBSC014","Increase passenger loading on afternoon peak","Service", "Contitionally Approved") );
    rows.push( application(1056,"SMBSC014","Increase passenger loading on morning peak","Service", "Contitionally Approved") );
    rows.push( application(1057,"SMBSC014","Increase passenger loading on afternoon peak","Service", "Contitionally Approved") );
    rows.push( application(1087,"SMBSC014","Increase passenger loading on afternoon peak","Service", "Contitionally Approved") );
    rows.push( application(1099,"SMBSC014","Increase passenger loading on morning peak","Service", "Contitionally Approved") );
    rows.push( application(1059,"SMBSC014","Increase passenger loading on afternoon peak","Service", "Contitionally Approved") );
    return rows
}

function approved() {
    var rows = []
    rows.push( application(1002,"SMBSC014","Time table adjustments for Blacktown","Service", "Approved") );
    rows.push( application(1003,"SMBSC014","Increase passenger loading on morning peak","Service", "Approved") );
    rows.push( application(901,"SMBSC014","Increase passenger loading on morning peak","Service", "Approved") );
    rows.push( application(902,"SMBSC014","Increase passenger loading on morning peak","Service", "Approved") );
    rows.push( application(903,"SMBSC014","Increase passenger loading on morning peak","Service", "Approved") );
    rows.push( application(904,"SMBSC014","Increase passenger loading on morning peak","Service", "Approved") );
    rows.push( application(905,"SMBSC014","Increase passenger loading on morning peak","Service", "Approved") );
    rows.push( application(906,"SMBSC014","Increase passenger loading on morning peak","Service", "Approved") );
    rows.push( application(907,"SMBSC014","Increase passenger loading on morning peak","Service", "Approved") );
    rows.push( application(908,"SMBSC014","Increase passenger loading on morning peak","Service", "Approved") );
    rows.push( application(804,"SMBSC014","Increase passenger loading on morning peak","Service", "Approved") );
    rows.push( application(805,"SMBSC014","Increase passenger loading on morning peak","Service", "Approved") );
    rows.push( application(806,"SMBSC014","Increase passenger loading on morning peak","Service", "Approved") );
    rows.push( application(807,"SMBSC014","Increase passenger loading on morning peak","Service", "Approved") );
    rows.push( application(808,"SMBSC014","Increase passenger loading on morning peak","Service", "Approved") );
    rows.push( application(908,"SMBSC014","Increase passenger loading on morning peak","Service", "Approved") );
    rows.push( application(804,"SMBSC014","Increase passenger loading on morning peak","Service", "Approved") );
    rows.push( application(805,"SMBSC014","Increase passenger loading on morning peak","Service", "Approved") );
    rows.push( application(806,"SMBSC014","Increase passenger loading on morning peak","Service", "Approved") );
    rows.push( application(807,"SMBSC014","Increase passenger loading on morning peak","Service", "Approved") );
    rows.push( application(808,"SMBSC014","Increase passenger loading on morning peak","Service", "Approved") );
    rows.push( application(908,"SMBSC014","Increase passenger loading on morning peak","Service", "Approved") );
    rows.push( application(804,"SMBSC014","Increase passenger loading on morning peak","Service", "Approved") );
    rows.push( application(805,"SMBSC014","Increase passenger loading on morning peak","Service", "Approved") );
    rows.push( application(806,"SMBSC014","Increase passenger loading on morning peak","Service", "Approved") );
    rows.push( application(807,"SMBSC014","Increase passenger loading on morning peak","Service", "Approved") );
    rows.push( application(808,"SMBSC014","Increase passenger loading on morning peak","Service", "Approved") );
    rows.push( application(908,"SMBSC014","Increase passenger loading on morning peak","Service", "Approved") );
    rows.push( application(804,"SMBSC014","Increase passenger loading on morning peak","Service", "Approved") );
    rows.push( application(805,"SMBSC014","Increase passenger loading on morning peak","Service", "Approved") );
    rows.push( application(806,"SMBSC014","Increase passenger loading on morning peak","Service", "Approved") );
    rows.push( application(807,"SMBSC014","Increase passenger loading on morning peak","Service", "Approved") );
    rows.push( application(808,"SMBSC014","Increase passenger loading on morning peak","Service", "Approved") );
    rows.push( application(908,"SMBSC014","Increase passenger loading on morning peak","Service", "Approved") );
    rows.push( application(804,"SMBSC014","Increase passenger loading on morning peak","Service", "Approved") );
    rows.push( application(805,"SMBSC014","Increase passenger loading on morning peak","Service", "Approved") );
    rows.push( application(806,"SMBSC014","Increase passenger loading on morning peak","Service", "Approved") );
    rows.push( application(807,"SMBSC014","Increase passenger loading on morning peak","Service", "Approved") );
    rows.push( application(808,"SMBSC014","Increase passenger loading on morning peak","Service", "Approved") );
    return rows
}


function rejected() {
    var rows = []
    rows.push( application(1021,"SMBSC014","Time table adjustments for Blacktown","Service", "Rejected") );
    rows.push( application(1022,"SMBSC015","Increase passenger loading on morning peak","Service", "Rejected") );
    rows.push( application(1021,"SMBSC014","Time table adjustments for Blacktown","Service", "Rejected") );
    rows.push( application(1022,"SMBSC015","Increase passenger loading on morning peak","Service", "Rejected") );
    return rows
}

function draft() {
    var rows = []
    rows.push( application(1024,"SMBSC014","Time table adjustments for Blacktown","Service", "Draft") );
    rows.push( application(1024,"SMBSC014","Time table adjustments for Wyong","Service", "Draft") );
    return rows
}

var alterationDescription = ["Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
                             "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?"]

function getAlterationDetails(id) {
    if (id < 1010) {
        return {id: id, requestDate: new Date(2013,5,16), description: alterationDescription[0]}
    } else {
        return {id: id, requestDate: new Date(2013,4,10), description: alterationDescription[1]}
    }
}

function getAlterationChanges(id) {
    var rows = []
    if (id < 1010) {
        rows.push( change("Additional Trips", "4901", "Wednesday", "Weekday School Term", "Morning Peak", "Standard", 2, 90, 140))
        rows.push( change("Additional Trips", "4901", "Wednesday", "Weekday School Term", "Afternoon Peak", "Standard", 2, 90, 140))
        rows.push( change("Additional Trips", "4903", "Thursday", "Weekday School Term", "Morning Peak", "Standard", 2, 90, 140))
        rows.push( change("Additional Trips", "4903", "Thursday", "Weekday School Term", "Afternoon Peak", "Standard", 2, 90, 140))
        rows.push( change("Additional Trips", "4904", "Friday", "Weekday School Term", "Morning Peak", "Standard", 2, 90, 140))
        rows.push( change("Additional Trips", "4904", "Friday", "Weekday School Term", "Afternoon Peak", "Standard", 2, 90, 140))
    } else {
        rows.push( change("Additional Trips", "4901", "Tuesday", "Weekday School Term", "Post Midnight", "Standard", 1, 2000, 20))
        rows.push( change("Additional Trips", "4901", "Wednesday", "Weekday School Term", "Early Morning", "Standard", 2, 4000, 40))
        rows.push( change("Additional Trips", "4901", "Tuesday", "Weekday School Term", "Morning Peak", "Standard", 1, 2000, 20))
        rows.push( change("Additional Trips", "4901", "Thursday", "Weekday School Term", "Morning Peak", "Standard", 1, 2000, 20))
        rows.push( change("Additional Trips", "4901", "Monday", "Weekday School Term", "Morning Peak", "Standard", 2, 90, 140))
        rows.push( change("Additional Trips", "4901", "Monday", "Weekday School Term", "Afternoon Peak", "Standard", 2, 90, 140))
        rows.push( change("Additional Trips", "4901", "Tuesday", "Weekday School Term", "Morning Peak", "Standard", 2, 90, 140))
        rows.push( change("Additional Trips", "4901", "Tuesday", "Weekday School Term", "Afternoon Peak", "Standard", 2, 90, 140))
        rows.push( change("Additional Trips", "4901", "Wednesday", "Weekday School Term", "Morning Peak", "Standard", 2, 90, 140))
        rows.push( change("Additional Trips", "4901", "Wednesday", "Weekday School Term", "Afternoon Peak", "Standard", 2, 90, 140))
        rows.push( change("Additional Trips", "4901", "Thursday", "Weekday School Term", "Morning Peak", "Standard", 2, 90, 140))
        rows.push( change("Additional Trips", "4901", "Thursday", "Weekday School Term", "Afternoon Peak", "Standard", 2, 90, 140))
        rows.push( change("Additional Trips", "4901", "Friday", "Weekday School Term", "Morning Peak", "Standard", 2, 90, 140))
        rows.push( change("Additional Trips", "4901", "Friday", "Weekday School Term", "Afternoon Peak", "Standard", 2, 90, 140))
    }



    return rows
}

function application(requestId, contract, subject, type, status) {
    return {requestId: requestId, contract: contract, subject: subject, type: type, status:status, created: new Date()}
}

function change(title, route, day, term, timeOfDay, busType, serviceTrips, serviceKms, serviceDuration) {
    return {change:title, route:route, day:day, term:term, timeOfDay:timeOfDay, busType:busType, serviceTrips:serviceTrips,
        serviceKms:serviceKms, serviceDuration:serviceDuration}
}